# PneuZ CRUD
Projeto desenvolvido para testes

## Como Rodar o Projeto
Para iniciar o projeto, é necessário clonar o repositório. Para isso, é preciso ter uma ferramenta de versionamento instalada. 

### Como instalar a Ferramenta de Versionamento git
Para instalar a ferramenta, [siga os seguintes passos](https://git-scm.com/book/pt-br/v2/Come%C3%A7ando-Instalando-o-Git "Instruções para instalação do git em pt-BR").

### Como instalar NodeJs
Para instalar a ferramenta, [siga os seguintes passos](https://nodejs.org/en/download/package-manager/ "Instruções para instalação do NodeJS em inglês")

### Já tenho git e NodeJS instalado em minha máquina
Podemos passar para o passo de clonagem do repositório.

#### Clonagem do Repositório
Usando o terminal, entre em uma pasta de sua preferência.

##### Clonando usando HTTPS
Usando o terminal, escreva o seguinte:

`git clone https://g_felicio@bitbucket.org/g_felicio/pneuzcrud.git`

A clonagem do repositório pode demorar algum tempo. Apenas espere o processo terminar.

##### Clonando usando SSH
Usando o terminal, escreva o seguinte:

`git clone git@bitbucket.org:g_felicio/pneuzcrud.git`

A clonagem do repositório pode demorar algum tempo. Apenas espere o processo terminar.

#### Repositório Clonado!
Primeiramente devemos inicializar o `composer` e o `NodeJS`, que foram usados nesse projeto.

Rode, primeiramente, usando o terminal na raiz do projeto:

`composer install`

Esse comando pode demorar alguns minutos até ser finalizado.

Após rodar o comando anterior, rode:

`npm install`

Esse comando pode demorar alguns minutos até ser finalizado.

Após rodar esses dois comandos, o repositório está praticamente inicializado.

